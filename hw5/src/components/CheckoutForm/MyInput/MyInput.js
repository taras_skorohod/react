import React from "react";
import "../CheckoutForm.scss"
const MyInput = ({placeholder, type, required, field, form}) => {
    const {errors, touched} = form;
    const {name} = field;
    return (
        <>
            <input  className="BuyingForm__field"
                type={type}
                   placeholder={placeholder}
                   required={required}
                {...field}/>
            {
                touched[name] && errors[name] && <span className={"BuyingForm__error"}>{errors[name]}</span>
            }
            </>
    )
};

export default MyInput;