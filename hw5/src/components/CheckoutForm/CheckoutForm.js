import React, {useState} from 'react';
import "./CheckoutForm.scss"
import {Formik, Form, Field} from "formik";
import * as yup from "yup";
import MyInput from "./MyInput/MyInput";
import '../Cart/Cart.css';
import Cart from "../Cart/Cart";

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const formSchema = yup.object().shape({
    userName: yup
        .string()
        .required("This field is required"),
    surName: yup
        .string()
        .required("This field is required"),
    age: yup
        .number()
        .required("This field is required")
        .lessThan(101, "Age must be real!")
        .moreThan(1, "Age must be real!")
        .round('floor'),
    address: yup
        .string()
        .required("This field is required")
        .max(30, "maximum 30 symbols"),
    phone: yup
        .string()
        .required("This field is required")
        .matches(phoneRegExp, 'Phone number is not valid')
        .min(9)
        .max(10)
});

const CheckoutForm = () => {

    const [form, setForm] = useState(true);

    return (
        form ?
            <>
                <h3 className={"BuyingForm__title"}>BUY NOW &#11015;</h3>
                <Formik
                    initialValues={{
                        userName: '',
                        surName: '',
                        age: '',
                        address: '',
                        phone: ''
                    }}

                    validationSchema={formSchema}

                    onSubmit={(values, {setSubmitting}) => {
                        setTimeout(() => {
                            console.log(`Customer info - ${JSON.stringify(values, null, 2,)} Selected Laptops: ${localStorage.getItem('AllCartProducts')}`);
                            localStorage.removeItem('AllCartProducts');
                            setSubmitting(false);
                            alert(`Dear ${values.userName}\nThanks for choose US\nWe will call you as soon as possible`);
                            setForm(false);
                        }, 400);
                    }}
                >
                    {({isSubmitting}) => (
                        <Form className="BuyingForm__container" noValidate>
                            <Field
                                name="userName"
                                component={MyInput}
                                type="text"
                                placeholder="Enter your Name"
                                required={true}
                            />
                            <Field
                                name="surName"
                                component={MyInput}
                                type="text"
                                placeholder="Enter your Surname"
                                required={true}
                            />
                            <Field
                                name="age"
                                component={MyInput}
                                type="number"
                                placeholder="Enter your Age"
                                required={true}
                            />
                            <Field
                                name="address"
                                component={MyInput}
                                type="text"
                                placeholder="Enter your address"
                                required={true}
                            />
                            <Field
                                name="phone"
                                component={MyInput}
                                type="number"
                                placeholder="Phone +38___"
                                required={true}
                            />
                            <button
                                type="submit"
                                disabled={isSubmitting}
                                className="BuyingForm__button"
                            >Send
                            </button>

                        </Form>
                    )}
                </Formik>
            </>
            : <Cart/>
    )
};


export default (CheckoutForm);