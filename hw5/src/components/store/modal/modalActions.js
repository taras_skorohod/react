import React from "react";
import Modal from "../../Modal/Modal";
import {addToCart, removeFromCart} from "../cart/cartActions";

export const SHOW_MODAL = "SHOW_MODAL";
export const CLOSE_MODAL = "CLOSE_MODAL";
export const OPEN_MODAL = "OPEN_MODAL";


export const closeModal = () => dispatch => {
    dispatch({type: CLOSE_MODAL})
};
export const showModal = () => dispatch => {
    dispatch({type: OPEN_MODAL})
};

export const RemoveFromCartModal = elem => dispatch => {
    dispatch({
        type: SHOW_MODAL,
        modal:
            <Modal header={"УДАЛИТЬ ИЗ КОРЗИНЫ"} text={"\n" +
            "Товар будет удален из корзины!"}
                   closeButton={true}
                   actions={
                       <>
                           <button className="modal__buttons-btn" onClick={() => {
                               dispatch(removeFromCart(elem));
                               dispatch(closeModal())
                           }}> OK
                           </button>
                           <button className="modal__buttons-btn" onClick={() => dispatch(closeModal())}> Отмена
                           </button>
                       </>
                   }/>
    })
};

export const AddToCartModal = elem => dispatch => {
    dispatch({
        type: SHOW_MODAL,
        modal:
            <Modal header={"Добавить в корзину"} text={"\n" +
            "Tовар будет добавлен в корзину"}
                   closeButton={true}
                   actions={
                       <>
                           <button className="modal__buttons-btn" onClick={() => {
                               dispatch(addToCart(elem));
                               dispatch(closeModal())
                           }}> OK
                           </button>
                           <button className="modal__buttons-btn" onClick={() => dispatch(closeModal())}> Отмена
                           </button>
                       </>
                   }/>
    })
};