import {SHOW_MODAL, CLOSE_MODAL, OPEN_MODAL} from "./modalActions";
import initialStore from "../initialStore";

export default function modalReducer(myStore = initialStore.modal, {type, modal}) {
    switch (type) {
        case SHOW_MODAL:
            return modal;
        case OPEN_MODAL:
            return modal;
        case CLOSE_MODAL:
            return null;
        default:
            return myStore
    }
}