import React from 'react';
import './App.scss';
import Footer from "./components/Footer/Footer";
import {NavLink} from "react-router-dom";
import AppRoutes from "./components/AppRoutes/routes";

const App = () => {

    return (
        <div className="App">

            <header className="header">
                <h1 className="header-title">\*_*/ закос под РАЗЕТКУ</h1>
                <NavLink className="menu-link" exact activeClassName="active" to={'/'}>Главная</NavLink>
                <NavLink className="menu-link" exact activeClassName="active" to={'/Cart'}>Корзина</NavLink>
                <NavLink className="menu-link" exact activeClassName="active" to={'/Favorites'}>Понравившееся</NavLink>
                <NavLink to={'/Cart'}><p className="header-cart"><i className="fas fa-shopping-cart"/></p></NavLink>
            </header>

            <AppRoutes/>

            <Footer/>

        </div>
    );
};


export default App;