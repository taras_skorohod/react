import React, {useState, useEffect} from 'react';
import "./Main.scss"
import ProductItem from "../ProductItem/ProductItem";
import PropTypes from 'prop-types';


const Main = (props) => {

    const {onClick} = props;
    const [items, setItems] = useState([]);


    useEffect(() => {
        async function fetchData() {
            const resp = await fetch("./productsStore.json");
            const data = await resp.json();
            setItems(data);
        }

        fetchData();
    }, []);


    const products = items.map((el, index) =>
        <ProductItem
            name={el.name}
            price={parseFloat(el.price)}
            url={el.url}
            article={el.article}
            color={el.color}
            onClick={onClick}
            key={index}
        />);

    return (
        <div className="main">
            {products}
        </div>
    );
};

export default Main;


Main.propTypes = {
    onClick: PropTypes.func.isRequired
}