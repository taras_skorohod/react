import React from 'react';
import FavoritesItem from "./FavoritesItem/FavoritesItem";
import "./Favorites.scss"

const Favorites = () => {

    if (localStorage.getItem('Favorites') === null) {
        return (
            <h2 className="favorites__section-text">Здесь пока пусто...</h2>
        )
    } else {
        let productsArr = JSON.parse(localStorage.getItem('Favorites')).map((el, index) =>
            <FavoritesItem
                name={el.name}
                price={parseFloat(el.price)}
                url={el.url}
                article={el.article}
                color={el.color}
                key={index}
            />);

        return (
            productsArr[0] === undefined
                ? <h2 className="favorites__section-text">Здесь пока пусто...</h2>
                : <div className="main">
                    {productsArr}
                </div>
        )
    }

};

export default Favorites;