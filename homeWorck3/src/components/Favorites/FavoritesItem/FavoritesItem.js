import React, {useState} from "react";
import "../../ProductItem/ProductItem.scss";
import PropTypes from "prop-types";
import CartItem from "../../Cart/CartItem/CartItem";

const FavoritesItem = (props) => {
    const [activeProduct, sectactiveProduct] = useState(true);
    const {name, price, url, article, color} = props;

    const removeFromFavorite = () => {
        let arr = JSON.parse(localStorage.getItem('Favorites'));
        localStorage.removeItem(`Favorites Product ${article}`);
        const newArr = arr.filter((item => item.article !== article));
        localStorage.setItem('Favorites', JSON.stringify(newArr));
        sectactiveProduct(false);
    }

    return (
        <>
            {activeProduct
                ?
                <div className="product-item__container">
                    <img className="product-item__photo" src={url} alt={"laptop_photo"}/>
                    <p className="product-item__name">{name}</p>
                    <p className="product-item__price">Цена - {price}$</p>
                    <p className="product-item__article">Номер товара - {article}</p>
                    <p className="product-item__color">Цвет - {color}</p>
                    <p className="product-item__choose-star-gold active">
        <i className="far fa-star" onClick={removeFromFavorite}/></p>
                </div>
                :
                ''
            }

        </>
    );
}

export default FavoritesItem;




CartItem.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.number,
    url: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string
};

CartItem.defaultProps = {
    price: 'not available',
    color: 'not available'
};
