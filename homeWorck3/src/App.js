import React from 'react';
import {useState} from 'react';
import './App.scss';
import Modal from './components/Modal/Modal';
import {NavLink} from "react-router-dom";
import AppRoutes from "./components/AppRoutes/routes";


const App = () => {
    const [modal, setModal] = useState(false);

    const showModal = (event) => {
        setModal(true);
    };

    const closeModal = () => {
        setModal(false);
    };

    return (
        <div className="App">
            <header className="header">
                <h1 className="header-title">ЗАКОС ПОД РОЗЕТКУ))</h1>
                <NavLink className="menu-link" exact activeClassName="active" to={'/'}>Главная</NavLink>
                <NavLink className="menu-link" exact activeClassName="active" to={'/Cart'}>Корзина</NavLink>
                <NavLink className="menu-link" exact activeClassName="active" to={'/Favorites'}>Избранное</NavLink>
                <NavLink to={'/Cart'}><p className="header-cart"><i className="fas fa-shopping-cart"/></p></NavLink>
            </header>

            {modal
                ? <Modal header={"успех"} text={"\n" +
                "Добавить в корзину"} closeButton={true} actions={
                        <button
                            className="modal__buttons-btn"
                            onClick={(event) => closeModal(event)}>
                            продолжить покупки
                        </button>
                } onClick={closeModal}/>
                : ''}

            <AppRoutes onClick={showModal}/>



        </div>
    );
};

export default App;