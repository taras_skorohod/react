import React from "react";
import {CartItem} from "./CartItem";
import {render} from "@testing-library/react"
import userEvent from "@testing-library/user-event";

const {click} = userEvent;

describe("Unit testing CartItem in the Cart", () => {

    test("smoke test remove-fromCartBtn",()=>{
        const {getByTestId} = render(<CartItem name={''} article={''} url={''}/>);
        const removeBtn = getByTestId("remove-fromCartBtn");
        expect(removeBtn).toHaveClass("product-item-remove-btn");
        expect(removeBtn).toHaveTextContent("X");
    })

    test("when we click remove-fromCartBtn - function is works", () => {
        const removeFromCart = jest.fn();
        const {getByTestId} = render(<CartItem name={''} article={''} url={''} RemoveFromCart={removeFromCart}/>)
        click(
            getByTestId("remove-fromCartBtn")
        )
        expect(removeFromCart).toBeCalled();
    })
})