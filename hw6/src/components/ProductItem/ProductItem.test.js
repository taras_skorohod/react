import React from "react";
import {ProductItem} from "./ProductItem";
import {render} from "@testing-library/react"
import userEvent from "@testing-library/user-event";

const {click} = userEvent;

describe("Unit testing ProductItem on Main Page", () => {
    test("smoke test product-item-buyBtn", () => {
        const {getByTestId} = render(<ProductItem name={''} url={''} article={''} elem={{}}/>)
        const buyBtn = getByTestId("product-item-buyBtn");
        expect(buyBtn.textContent).toBe("Add to cart");
        expect(buyBtn.className).toBe("product-item-buyBtn");

    })

    test("when we click product-item-buyBtn - function is works", () => {
        const showBuyingModal = jest.fn();
        const {getByTestId} = render(<ProductItem name={''} url={''} article={''} elem={{}}
                                                  AddToCartModal={showBuyingModal}/>)
        expect(showBuyingModal).not.toBeCalled();

        click(
            getByTestId("product-item-buyBtn")
        )
        expect(showBuyingModal).toBeCalled();
    });

    test("smoke test add-to-favoritesBtn", () => {
        const {getByTestId} = render(<ProductItem name={''} url={''} article={''} elem={{}}/>);
        const addToFavBtn = getByTestId("add-to-favoritesBtn");
        expect(addToFavBtn.className).toBe("product-item__choose-star");

    })

    test("when we click add-to-favoritesBtn || remove-from-favoritesBtn - function is works", () => {
        const addToStorage = jest.fn();
        const removeFromStorage = jest.fn();
        const {getByTestId} = render(<ProductItem
            name={''}
            url={''}
            article={''}
            elem={{}}
            addToFavourites={addToStorage}
            removeFromFavourites={removeFromStorage}/>)

        click(
            getByTestId("add-to-favoritesBtn")
        )

        expect(addToStorage).toBeCalled();

        const removeFromFavBtn = getByTestId("remove-from-favoritesBtn");
        expect(removeFromFavBtn.className).toBe("product-item__choose-star-gold");

        click(removeFromFavBtn);
        expect(removeFromStorage).toBeCalled();
    })

});