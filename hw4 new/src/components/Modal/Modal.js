import React from 'react';
import './Modal.scss';
import PropTypes from 'prop-types';
import {closeModal} from "../store/modal/modalActions";
import {connect} from "react-redux";

const Modal = ({header, text, actions, closeButton,closeModal}) =>  {

        return (
            <>
                <div className="modal__container">
                    {closeButton ? <button className="modal__container-closeBtn" onClick = {()=>closeModal()}>X</button> :''}
                    <h2 className="modal__container-title">{header}</h2>
                    <p className="modal__container-text">{text}</p>
                    <div className="modal__buttons">{actions}</div>
                </div>
                <div className="modal-wrapper" onClick = {()=>closeModal()}/>
            </>
        );
    };

const mapStoreToProps = (store) => ({
    myModal: store.modal
});

const mapDispatchToProps = (dispatch) => {
    return {
        closeModal: () => dispatch(closeModal())
    }
};

export default connect(mapStoreToProps,mapDispatchToProps)(Modal);

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  closeButton:PropTypes.bool
};
