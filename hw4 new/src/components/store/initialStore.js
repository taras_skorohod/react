const initialStore = {
    modal: null,
    items: {
        loading: false,
        error: null,
        result: []
    },
    AllCartProducts:[],
    AllFavoritesProducts:[]
};

export default initialStore;