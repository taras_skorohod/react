import {LOAD_CART,ADD_TO_CART,REMOVE_FROM_CART} from "./cartActions";
import initialStore from "../initialStore";

export default function cartItemsReducer
    (cartStore = initialStore.AllCartProducts, {AllCartProducts, type}) {
    switch (type) {
        case LOAD_CART:
         return AllCartProducts;
        case ADD_TO_CART:
            return [];
        case REMOVE_FROM_CART:
            return AllCartProducts;
        default:
            return cartStore;
    }
}