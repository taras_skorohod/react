import {ITEMS_LOADING_START, ITEMS_LOADING_END, ITEMS_LOADING_ERROR} from "./itemsActions";
import initialStore from "../initialStore";

export default function itemsReducer(itemsStore = initialStore.items, {type, error, result}) {
    switch (type) {
        case ITEMS_LOADING_START:
            return {
                ...itemsStore,
                loading: true
            };
        case ITEMS_LOADING_END:
            return {
                ...itemsStore,
                loading: false,
                result
            };
        case ITEMS_LOADING_ERROR:
            return {
                ...itemsStore,
                loading: false,
                error
            };
        default:
            return itemsStore;
    }
}