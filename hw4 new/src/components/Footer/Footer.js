import React from 'react';
import './Footer.scss'

const Footer = () => {

    return (
        <footer className="footer">
            <p>&copy; Copyright 2021 | All Rights
                Reserved
            </p>
        </footer>
    );
};


export default Footer;