import React from 'react';
import './Cart.css';
import CartItem from "./CartItem/CartItem";

const Cart = () => {

    let productsArr = JSON.parse(localStorage.getItem('AllCartProducts'));
    if (localStorage.getItem('AllCartProducts') === null || productsArr[0] === undefined) {
        return (
            <h2 className="cart__section-text">There is no products in the Cart</h2>
        )
    } else {
        productsArr = JSON.parse(localStorage.getItem('AllCartProducts')).map((el, index) =>
            <CartItem
                name={el.name}
                price={parseFloat(el.price)}
                url={el.url}
                article={el.article}
                color={el.color}
                key={index}
            />);
        return (
                    <div className="cart__products">
                        {productsArr}
                    </div>
        )
    }
};

export default Cart;

