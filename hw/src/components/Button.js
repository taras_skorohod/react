const Button = props => {
    const {background, onClick, text} = props;
    return (
        <button
            className={'btn'}
            style={{backgroundColor: background}}
            onClick={onClick}>
            {text}
        </button>
    )
}
export default Button
