const Modal = props => {
    const {close, isOpened, style, onModalClose, closeButton, header, text, action} = props;
    return (
        <div
            onClick={close}
            className={`modal__wrapper ${isOpened ? 'open' : 'close'}`} style={{ ...style }}
        >
            <div
                className='modal__body'
                onClick={e => e.stopPropagation()}
            >
                {closeButton&&<div className='modal__close' onClick={onModalClose}> x </div>}
                <h2>{header}</h2>
                <hr />
                {text}
                <div>
                    {action}
                </div>
            </div>
        </div>
    )
}

export default Modal
