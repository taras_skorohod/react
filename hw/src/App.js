import './App.scss';
import Modal from './components/Modal';
import Button from './components/Button';
import React  from "react";

class App extends React.Component {

  state = {
    modal1: false,
    modal2: false,
  }

  close = () => {
    console.log('close');
    this.setState({
      modal1: false,
      modal2: false,
    })
  }

  openFirstModal = () =>{
    this.setState({
      ...this.state,
      modal1: true,
    })
  }

  openSecondModal = () =>{
    this.setState({
      ...this.state,
      modal2: true,
    })
  }
  render() {

    return (
        <div className="App">
          <header className="App-header">
            <div>
              <Button
                  text ={'Open first modal'}
                  background = {'#79a871'}
                  onClick={this.openFirstModal}
              />
              <Button
                  text ={'Open second modal'}
                  background = {'#4d69bb'}
                  onClick={this.openSecondModal}
              />
            </div>
          </header>
          <Modal
              close={this.close}
              closeButton={false}
              header={'window?'}
              text={'=)'}
              isOpened={this.state.modal1}
              onModalClose={() => this.setState({ ...this.state, modal1: false })}
              action = {
                [<Button
                    text ={'well done'}
                    background = {'#48b530'}
                />
                  ,
                  <Button
                      text ={'OK'}
                      background = {'#f54d4d'}
                  />]
              }
          />
          <Modal
              close={this.close}
              closeButton={true}
              header={'window?'}
              isOpened={this.state.modal2}
              onModalClose={() => this.setState({ ...this.state, modal2: false })}
              action = {
                [<Button
                    text ={')'}
                    background = {'#a456c6'}
                />
                ]}
          />

        </div>
    )
  };
}

export default App;
